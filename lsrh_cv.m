function params = lsrh_cv(train_data, Nb, opts_K, opts_lambda, opts_beta, Nfold)
%% Cross validate on the paramters: K, lambda, and beta for CMRSH
X = train_data.X;
Y = train_data.Y;
S = train_data.S;

if nargin == 5; Nfold = 3; end
% choose a small subset to speed up validation
N = size(X, 2);
maxValSet = 900;
if N > maxValSet
    N = maxValSet;
    X = X(:, 1:maxValSet);
    S = S(1:maxValSet, 1:maxValSet);
end

Ntest = floor(N / Nfold);
params.K = opts_K(1);
params.lambda = opts_lambda(1);
params.beta = opts_beta(1);
%% Cross-validate lambda
if numel(opts_K) > 1
    apK = zeros(Nfold, numel(opts_K));
    fprintf('Cross validate LSRH over K @ Nb = %d.\n', Nb);   
    for iter = 1 : Nfold   
        % split training data into training set & cross validation set        
        teId = zeros(1, N); teId((iter-1)*Ntest+1:iter*Ntest) = 1; teId = teId == 1;
        trId = ~teId;
        train_data.X = X(:, trId);
        train_data.Y = Y(:, trId);
        test_data.X = X(:, teId);
        test_data.Y = Y(:, teId);
        train_data.S = S(trId, trId);
        test_data.S = S(teId, trId);
        fprintf('-----------------Iteration %d----------------\n', iter);    
        for i = 1 : numel(opts_K)        
            opts.K = opts_K(i);
            opts.L = ceil(Nb/ceil(log2(opts.K)));
            opts.lambda = 1.0;
            opts.beta = 1.0;
            
            % train with current parameter setting
            [train_code, model] = lsrh(train_data, opts);
            % compute hash code of test set
            [test_code, ~] = lsrh(test_data, opts, model);
            
            Hx = train_code.Hx; Hy = train_code.Hy;
            Hxt = test_code.Hx; Hyt = test_code.Hy;
            
            % Text query image             
            Dyx = pdist2(Hyt', Hx', 'hamming')*opts.L;
            [ap_yx, ~, ~, ~, ~] = test_hash(Dyx, test_data.S, opts.L, [50]);
            % Image query text
            Dxy = pdist2(Hxt', Hy', 'hamming')*opts.L;
            [ap_xy, ~, ~, ~, ~] = test_hash(Dxy, test_data.S, opts.L, [50]);             
            apK(iter, i) = (ap_xy + ap_yx) / 2;
            fprintf('lambda = 1.0/K = %d----->ap_yx = %.4f/ap_xy = %.4f\n', opts.K, ap_yx, ap_xy);                       
        end
    end
    [bestAP, kid] = max(mean(apK, 1));
    params.K = opts_K(kid);
    fprintf('Best ap = %.4f @ K = %d\n', bestAP, params.K);
end
%% Cross-validate lambda
if numel(opts_lambda) > 1
    fprintf('Cross validate LSRH over lambda.\n');
    ap = zeros(Nfold, numel(opts_lambda));
    L = ceil(Nb/ceil(log2(params.K)));
    for iter = 1 : Nfold
        fprintf('-----------------Iteration %d----------------\n', iter);
        % split training data into training set & cross validation set        
        teId = zeros(1, N); teId((iter-1)*Ntest+1:iter*Ntest) = 1; teId = teId == 1;
        trId = ~teId;
        train_data.X = X(:, trId);
        train_data.Y = Y(:, trId);
        test_data.X = X(:, teId);
        test_data.Y = Y(:, teId);
        train_data.S = S(trId, trId);
        test_data.S = S(teId, trId);
        for k = 1 : numel(opts_lambda)                                
            opts.K = params.K;
            opts.L = L;
            opts.lambda = opts_lambda(k);
            opts.beta = 1.0;
            
            % train with current parameter setting
            [train_code, model] = lsrh(train_data, opts);
            % compute hash code of test set
            [test_code, ~] = lsrh(test_data, opts, model);
            
            Hx = train_code.Hx; Hy = train_code.Hy;
            Hxt = test_code.Hx; Hyt = test_code.Hy;
            
            % Text query image
            Dyx = pdist2(Hyt', Hx', 'hamming')*opts.L;
            [ap_yx, ~, ~, ~, ~] = test_hash(Dyx, test_data.S, opts.L, [50]);
            % Image query text
            Dxy = pdist2(Hxt', Hy', 'hamming')*opts.L;
            [ap_xy, ~, ~, ~, ~] = test_hash(Dxy, test_data.S, opts.L, [50]);             
            ap_t = (ap_xy + ap_yx) / 2;
            ap(iter, k) = ap_t; 
            fprintf('K = %d/lambda = %.2f----->ap_xy = %.4f/ap_yx = %.4f\n', params.K, opts.lambda, ap_xy, ap_yx);               
        end        
    end
    ap_mean = mean(ap, 1);
    [params.bestAP, idx] = max(ap_mean);    
    params.lambda = opts_lambda(idx);
    fprintf('Best ap = %.4f @ lambda = %f\n', params.bestAP, params.lambda);
end
%% Cross-validate beta
if numel(opts_beta) > 1
    fprintf('Cross validate LSRH over beta.\n');
    ap = zeros(Nfold, numel(opts_beta));
    L = ceil(Nb/ceil(log2(params.K)));
    for iter = 1 : Nfold
        fprintf('-----------------Iteration %d----------------\n', iter);
        % split training data into training set & cross validation set        
        teId = zeros(1, N); teId((iter-1)*Ntest+1:iter*Ntest) = 1; teId = teId == 1;
        trId = ~teId;
        train_data.X = X(:, trId);
        train_data.Y = Y(:, trId);
        test_data.X = X(:, teId);
        test_data.Y = Y(:, teId);
        train_data.S = S(trId, trId);
        test_data.S = S(teId, trId);
        for k = 1 : numel(opts_beta)                    
            opts.K = params.K;
            opts.L = L;
            opts.lambda = params.lambda;
            opts.beta = opts_beta(k);
            
            % train with current parameter setting
            [train_code, model] = lsrh(train_data, opts);
            % compute hash code of test set
            [test_code, ~] = lsrh(test_data, opts, model);
            
            Hx = train_code.Hx; Hy = train_code.Hy;
            Hxt = test_code.Hx; Hyt = test_code.Hy;
            
            % Text query image
            Dyx = pdist2(Hyt', Hx', 'hamming')*opts.L;
            [ap_yx, ~, ~, ~, ~] = test_hash(Dyx, test_data.S, opts.L, [50]);
            % Image query text
            Dxy = pdist2(Hxt', Hy', 'hamming')*opts.L;
            [ap_xy, ~, ~, ~, ~] = test_hash(Dxy, test_data.S, opts.L, [50]);             
            ap_t = (ap_xy + ap_yx) / 2;
            
            ap(iter, k) = ap_t; 
            fprintf('K = %d/beta = %.2f----->ap_xy = %.4f/ap_yx = %.4f\n', params.K, opts.beta, ap_xy, ap_yx);               
        end        
    end
    ap_mean = mean(ap, 1);
    [params.bestAP, idx] = max(ap_mean);    
    params.beta = opts_beta(idx);
    fprintf('Best ap = %.4f @ beta = %f\n', params.bestAP, params.beta);
end
